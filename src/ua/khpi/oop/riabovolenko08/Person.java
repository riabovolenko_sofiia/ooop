package ua.khpi.oop.riabovolenko08;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Person {
	private String name;
	private ArrayList<String> telephone;
	private Calendar dateOfBirthday;
	private String address;
	private Date dateOfLastRedaction;
	/**
     * Метод дати, який дозволяє вивести дату останього редагування
     */
	public void setDateOfLastRedaction(Date dateOfLastRedaction) {
		this.dateOfLastRedaction = dateOfLastRedaction;
	}
	/**
     * Метод setname, який дозволяє додати ім'я адресата
     */
	public void setName(String name) {
		this.name = name;
		dateOfLastRedaction = new Date();
	}
	/**
     * Метод getName, який дозволяє повернути ім'я адресата
     */
	public String getName() {
		return name;
	}
	/**
     * Метод settelephone, який дозволяє додати номер телефону адресата
     */
	public void setTelephone(ArrayList<String> telephone) {
		this.telephone = telephone;
		dateOfLastRedaction = new Date();
	}
	/**
     * Метод getTelephone, який дозволяє повернути номер телефону адресата
     */
	public ArrayList<String> getTelephone() {
		return telephone;
	}
	/**
     * Метод setDateofBirthday, який дозволяє додати день народження адресата
     */
	public void setDateOfBirthday(Calendar dateOfBirthday) {
		this.dateOfBirthday = dateOfBirthday;
		dateOfLastRedaction = new Date();
	}
	/**
     * Метод getDateOfBirthday, який дозволяє повернути день народження адресата
     */
	public Calendar getDateOfBirthday() {
		return dateOfBirthday;
	}
	/**
     * Метод setaddress, який дозволяє додати адресу адресата
     */
	public void setAddress(String address) {
		this.address = address;
		dateOfLastRedaction = new Date();
	}
	/**
     * Метод getAddress, який дозволяє повернути адресу адресата
     */
	public String getAddress() {
		return address;
	}
	/**
     * Метод getDateOfLastRedaction, який дозволяє повернути дату останього редагування адресата
     */
	public Date getDateOfLastRedaction() {
		return dateOfLastRedaction;
	}
	/**
     * Метод getDateOfLastRedaction, який дозволяє додати ще один телефонний номер адресата
     */
	public void addTelephone(String telephone) {
		this.telephone.add(telephone);
		dateOfLastRedaction = new Date();
	}
	
	public Person() {
		super();
	}
	Person(String name,ArrayList<String> telephone,Calendar dateOfBirthday,String address){
		this.setName(name);
		this.setTelephone(telephone);
		this.setDateOfBirthday(dateOfBirthday);
		this.setAddress(address);
	}
	Person(String name,String telephone,Calendar dateOfBirthday,String address){
		this.setName(name);
		ArrayList<String> mass= new ArrayList<>();
		mass.add(telephone);
		this.setTelephone(mass);
		this.setDateOfBirthday(dateOfBirthday);
		this.setAddress(address);
	}
	Person(String name,ArrayList<String> telephone,Calendar dateOfBirthday,String address, Date dateOfLastRedaction){ 
		this.setName(name);                                                                 
    	this.setTelephone(telephone);                                                       
    	this.setDateOfBirthday(dateOfBirthday);                                             
    	this.setAddress(address);                 
    	this.setDateOfLastRedaction(dateOfLastRedaction);
    }                                                                                       
	}
	