package ua.khpi.oop.riabovolenko03;
import java.util.Scanner; //scanner import

public class Main{
    /**
	* An entry point - main method.
	*
	* @param args - arguments of main method
	*/

    /**
    * Завдання.
    * Ввести текст. У кожному слові тексту k-ту літеру замінити заданим символом. 
    * Якщо k більше довжини слова, видати повідомлення та заміну не виконувати. 
    * Початкові дані та результат вивести у вигляді таблиці.
    */
    public static void main(String args[]) {
       Scanner wk_string = new Scanner(System.in); //Створення сканеру
       System.out.println("Enter string:"); 
       String string = wk_string.nextLine(); //Створення строки
       String firststring = string;
       System.out.println("Your string : " +string);    //Вивід результату
       System.out.println("Enter k:"); 
       int k_alphabet_n = wk_string.nextInt();  //k switching letter
       char k = 'k'; 
       char[] wk_char = string.toCharArray();
        int c_length = wk_char.length; //Знаходження довжини
        int j = 0 ; //Сounter літер
       for(int i = 0 ; i<c_length;i++){
       	if(wk_char[i]==' ' || wk_char[i]==',' || wk_char[i]=='.' || wk_char[i]==';'){
       	if(j<k_alphabet_n){System.out.println("Can`t change the letter");}	
       		j=0;
       		continue;
       	}
       	j++ ;
       	if(j==k_alphabet_n){wk_char[i]=k;}
       }
    System.out.println(firststring);
    System.out.println(wk_char);
    }
}
