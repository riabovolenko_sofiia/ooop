package ua.khpi.oop.riabovolenko04;

import java.util.Scanner; // импорт сканнера

public class Main {
    /**
	* Debug метод. Допомагає з виводом дебагу під час виконання програми
	*
	* @param i - позиція літери
	*/
    public static void debug(int i) {
        System.out.println("Current symbol position:" +i);
    }
    /**
	* help метод. Виводить інформацію про програму, автора, версію та завдання.
	*/
    private static  void help() {
        System.out.println("@author : Riabovolenko Sofiia KIT120B \nversion : 1.0.0\nEnter text: In each word of the text, replace the k-th letter with the specified character. If k is longer than") ;
        System.out.println("the word, do not replace. Output the initial data and the result in the form of a table");
        System.out.println("");
        System.out.println("1)Enter data;\n2)Check data that is entered;\n3)Do the operations and save resault;\n4)Show results of operations;");
    }
    /**
	* An entry point - main method.
	*
	* @param args - arguments of main method
	*/
    public static void main(String args[]) {
        String arg = args[0];
        if (arg.equals("-h") || arg.equals("-help")) {
            help();
        }
        System.out.println("\nChoose task :");
        System.out.println("\n1)Enter data \n2)Check data\n3)Do operation\n4)Show result");
        Scanner tasks = new Scanner(System.in);
        int task = tasks.nextInt();
          Scanner wk_string = new Scanner(System.in); //creating scanner
        System.out.println("Enter string:");
        String string = wk_string.nextLine(); //creating string  and scanning input
        String firststring = string; //copying first string for table output
        System.out.println("Enter k:"); 
        int k_alphabet_n = wk_string.nextInt();  // entering the number of letter to change
        char k = 'k';
        char[] wk_char = string.toCharArray();
        int c_length = wk_char.length; // getting char length
        int j = 0 ; // letters counter
        for(int i = 0 ; i<c_length;i++){
            if(wk_char[i]==' ' || wk_char[i]==',' || wk_char[i]=='.' || wk_char[i]==';'){
                if(j<k_alphabet_n){System.out.println("Can`t change letter");}
                j=0;
                continue;
            }
            j++ ;
            if(j==k_alphabet_n){wk_char[i]=k;}
        }
        while (task!=0) {
            System.out.println("\nPlease choose task :");
            System.out.println("\n1)Enter data \n2)Check data\n3)Math operation\n4)Show result");
            task = tasks.nextInt();
            switch (task) {
                case 2:
                    System.out.println("\nYour string : " + string);//showing input result
                    break;
                case 3:
                    for (int i = 0; i < c_length; i++) {
                        if (wk_char[i] == ' ' || wk_char[i] == ',' || wk_char[i] == '.' || wk_char[i] == ';') {
                            if (j < k_alphabet_n) {
                                System.out.println("Can`t change letter");
                            }
                            j = 0;
                            continue;
                        }
                        j++;
                        if (j == k_alphabet_n) {
                            debug(i);
                            wk_char[i] = k;
                        }
                    }
                    break;
                case 4:
                    System.out.println(firststring);
                    System.out.println(wk_char);
                    break;
            }
            }
    }
}

