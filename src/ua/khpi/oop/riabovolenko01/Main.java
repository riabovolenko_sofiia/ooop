import java.util.Arrays;
public class Main {
    public static void main(String[] args) {
    /**
	* An entry point - main method.
	*
	* @param args - arguments of main method
	*/

    /**
    * Завдання.
    * Вирішити три прикладні задачі на мові Java в середовищі Eclipse.
    */
    
        First_task();
    }
    private static void First_task(){
    //Завдання 1
        short list_num = 0x13;
        long phone_num =380667128126L;
        int two_last = 0b11010;
        int four_last = 017676;
        int listpercent = (list_num-1)%26+1;
        int alphabet_num = 64+listpercent;
        System.out.println("Task 1");
        System.out.println("List number is: " + list_num);
        System.out.println("Phone number is: " + phone_num);
        System.out.println("Two last numbers of phone number are: " + two_last);
        System.out.println("Two four numbers of phone number are: " + four_last);
        System.out.println("Division percentage is: " + listpercent);
        System.out.println("Ascii of number in last task is: " + (char)alphabet_num);
        Second_task(list_num, phone_num, two_last, four_last, listpercent, alphabet_num);
        Third_task(list_num, phone_num, two_last, four_last, listpercent, alphabet_num);
    }

    // Завдання 2
    private static void Second_task(short list_num,long phone_num,int two_last,int four_last,int listpercent ,int alphabet_num){
        long nums [] = {list_num,phone_num,two_last,four_last,listpercent ,alphabet_num};
        int even = 0;
        int odd = 0;
        for (int i = 0 ;i<5;i++) {
            if ((int) nums[i] % 2 == 0) even++;
            else odd++;
        }
        System.out.println("_________\nTask 2");
        System.out.println("Amount of odd numbers: " + odd);
        System.out.println("Amount of even numbers: " + even);
    }

    // Завдання 3
    private static void Third_task(short list_num,long phone_num,int two_last,int four_last,int listpercent ,int alphabet_num){
        long nums [] = {list_num,phone_num,two_last,four_last,listpercent ,alphabet_num};
        System.out.println("_________\nTask 3");
        for(int i = 0 ; i<6 ; i++){
            String string = Long.toBinaryString(nums[i]);
            char[] array = string.toCharArray();
            int counter = 0;
            for (char c : array) {
                if (c == '1') counter++;
            }
            System.out.println("Number in binary: " + Arrays.toString(array));
            System.out.println(counter);
            array = null ;
        }
    }
}
