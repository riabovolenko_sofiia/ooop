package ua.khpi.oop.riabovolenko07;

import java.util.ArrayList;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;



public class AddressBook {
	ArrayList<Person> mass = new ArrayList<Person>();
	
	/**
     * Метод add, який дозволяє додати адресата
     */
	public void add(Person person) {
		mass.add(person);
	}
	
	/**
     * Метод add, який дозволяє вивести адресата
     */
	public void print() {
	for(var person : mass) {
		System.out.println("\nName: "+person.getName()+"\nTelephone: "+ person.getTelephone() + "\nDate of birthday: " +person.getDateOfBirthday().getTime()+ "\nAdress: " +person.getAddress()+"\nDate of last redaction: "+ person.getDateOfLastRedaction().toString());
		}
	}
	
	
	
	
	
	/**
     * Метод add, який дозволяє зберегти адресата, а якщо ні - FileNotFoundException
     */
	public void save(String fileName) throws FileNotFoundException {
		XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(fileName)));
		encoder.writeObject(mass);
		encoder.close(); 
		System.out.println("Serialization successful\n");
	}
	
	@SuppressWarnings("unchecked")
	public void download(String fileName) throws FileNotFoundException {
		XMLDecoder d = new XMLDecoder(new BufferedInputStream(new FileInputStream(fileName)));
		mass = (ArrayList<Person>) d.readObject();
		d.close();
		System.out.println("Serialization successful\n");
	}
	/**
     * Метод delete, який дозволяє видалити адресата
     */
	void delete(int n) {
		if(n<mass.size())
		mass.remove(n);
		else
			System.out.println("Element don`t exists");
	}
	/**
     * Метод AddressBook, який дозволяє створити нову адресну книжку
     */
	public AddressBook() {
		super();
		// TODO Auto-generated constructor stub
	}
	 
	 
}
