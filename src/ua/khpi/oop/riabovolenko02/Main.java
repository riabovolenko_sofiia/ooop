import java.util.Arrays;
import java.util.Random;

import javax.xml.namespace.QName;
public class Main {
    /**
	* An entry point - main method.
	*
	* @param args - arguments of main method
	*/

    /**
    * Завдання.
    * Знайти суму цифр заданого цілого числа.
    */
    public static void main(String args[]) {
        for (int i = 0; i < 10; i++){
            Random random = new Random();
            int result = 0;
            int num1 = random.nextInt(); //створюємо рандомне число
            System.out.println("Random number is: " + num1);
            int number_create;
        if (num1<0){ //робимо число +, якщо воно від'ємне
            num1 *=-1; 
        }
        for (;num1>0;num1 /=10){
            number_create = num1%10;
            result = result +number_create; //знаходимо сумму цифр числа
        }
        System.out.println("Sum of numbers is: " + result);
        }
    }
}